﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Book
    {
        //Id книги
        public int Id { get; set; }
        //Название книги
        public string Name { get; set; }
        //Автор книги
        public string Author { get; set; }
        //Издатель книги
        public string Publisher { get; set; }
        //Год издания
        public DateTime Year { get; set; }
        //Количество экземпляров
        public int NumberCopies { get; set; }
        //Цена книги
        public int Price { get; set; }
    }
}
