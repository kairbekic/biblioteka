﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Author
    {
        // ID автора
        public int AuthorId { get; set; }
        //Имя автора
        public string FirstName { get; set; }
        //Фамилия автора
        public string LastName { get; set; }

    }
}
